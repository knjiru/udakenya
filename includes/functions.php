<?php
include "dbconf.php";

function secureSessionStart() {
	$session_name = 'secSessionID'; // Set a custom session name
	$secure = false; // Set to true if using https.
	$httponly = true; // This stops javascript being able to access the session id.

	ini_set('session.use_only_cookies', 1); // Forces sessions to only use cookies.
	$cookieParams = session_get_cookie_params(); // Gets current cookies params.
	session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
	session_name($session_name); // Sets the session name to the one set above.
	session_start(); // Start the php session
	 session_regenerate_id(); // regenerated the session, delete the old one.
}

function login($IDNumber, $pswd) {
	global $dbConn;
	if($stmt1 = $dbConn->prepare("SELECT surname, IDNumber, pswd, salt FROM users WHERE IDNumber = :IDNumber LIMIT 1"))
		$stmt1->bindValue(':IDNumber', $IDNumber, PDO::PARAM_STR);
		$stmt1->execute();

		while($row1 = $stmt1->fetch()) {
			$surname = $row1['surname'];
			$IDNumber = $row1['IDNumber'];
			$dbPassword = $row1['pswd'];
			$salt = $row1['salt'];
	}

		$password = hash('sha512', $pswd.$salt);
		if($stmt1->rowCount() == 1) { // If the user exists
		// We check if the account is locked from too many login attempts
			if(checkBrute($IDNumber) == true) {
			// Account is locked. Send an email to user saying their account is locked
				return false;
			}else {
				if($dbPassword == $password) { // Check if the password in the database matches the password the user submitted.
					// Password is correct!
					$user_browser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.
					$surname = preg_replace("/[^0-9]+/", "", $surname); // XSS protection as we might print this value
					$_SESSION['surname'] = $surname;
					$username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $surname); // XSS protection as we might print this value
					$_SESSION['IDNumber'] = $IDNumber;
					$_SESSION['loginString'] = hash('sha512', $password.$user_browser);
					// Login successful.


					return true;
					}else{
		        // Password is not correct
		        // We record this attempt in the database
		        $now = date("Y-m-d H:i:s");
		        $stmt2 = $dbConn->prepare("INSERT INTO login_attempts (IDNumber, timeDone) VALUES (:IDNumber, :now)");
						$stmt2->bindValue(':IDNumber', $IDNumber, PDO::PARAM_STR);
						$stmt2->bindValue(':now', $now, PDO::PARAM_STR);
						$stmt2->execute();

		     }
		  }
	 }else{
		     // No user exists.
		 return false;
	 }
}

function getName($IDNumber) {

	global $dbConn;

	$stmt1 = $dbConn->prepare("select IDNumber, surname, firstName, otherNames  from users WHERE IDNumber = :IDNumber LIMIT 0,1");
		$stmt1->bindValue(':IDNumber', $IDNumber, PDO::PARAM_STR);
		$stmt1->execute();
		while($row1 = $stmt1->fetch()) {
			$IDNumber = $row1['IDNumber'];
			$surname = $row1['surname'];
			$firstName = $row1['firstName'];
			$otherNames = $row1['otherNames'];
		}
		$name = $surname . " ". $firstName . " ". $otherNames;
	return $name;
}


function checkBrute($IDNumber) {
	global $dbConn;
	$now = date("Y-m-d H:i:s");
	// All login attempts are counted from the past 2 hours.
	$validAttempts = time()- (2 * 60 * 60);

	if ($stmt1 = $dbConn->prepare("SELECT timeDone FROM login_attempts WHERE IDNumber = :IDNumber AND timeDone > :time")) {
		  $stmt1->bindValue(':IDNumber', $IDNumber, PDO::PARAM_STR);
			$stmt1->bindValue(':time', $validAttempts, PDO::PARAM_STR);
		  $stmt1->execute();
		  // If there has been more than 5 failed logins
		  if($stmt1->rowCount() > 5) {
		     return true;
		  } else {
		     return false;
		  }
	 }
}


function loginCheck() {


	global $dbConn;
	// Check if all session variables are set
	if(isset($_SESSION['surname'], $_SESSION['IDNumber'], $_SESSION['loginString'])) {

		$surname = $_SESSION['surname'];
		$loginString = $_SESSION['loginString'];
		$IDNumber = $_SESSION['IDNumber'];

		$user_browser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.

		if ($stmt1 = $dbConn->prepare("SELECT pswd FROM users WHERE IDNumber = ? LIMIT 1")) {
			$stmt1->bindValue(1, $IDNumber, PDO::PARAM_STR);
			$stmt1->execute(); // Execute the prepared query.
			while($row1 = $stmt1->fetch()) {
				$password = $row1['pswd'];
			}

			if($stmt1->rowCount() == 1) { // If the user exists
				$loginCheck = hash('sha512', $password.$user_browser);
				if($loginCheck == $loginString){
					// Logged In!!!!
					return true;
				}else{
					// Not logged in
					return false;
				}
			}else{
		   	// Not logged in
				return false;
			}
		}else {
			// Not logged in
			return false;
		}
	} else {
		// Not logged in
		return false;
	}
}


function getPage($id){
	global $dbConn;

//Is the user Logged in
	if(loginCheck() == true){

//In case an ID id not provided go to home page
		if($id == ""){
			$id = "hom";
		}

		if(getAuth($id,$_SESSION['IDNumber']) == true){
			$stmt1 = $dbConn->prepare("SELECT form FROM menu WHERE pageID = ? LIMIT 1");
			$stmt1->bindValue(1, $id, PDO::PARAM_STR);
			$stmt1->execute();
			while($row1 = $stmt1->fetch()){
				$page = $row1['form'];
			}

			return "view/".$page;

		}else{
			return "view/404.php";
		}
//Allow login in and registering
	}else {
		if($id == ""){
			$id = "mm";
		}

		$stmt2 = $dbConn->prepare("SELECT form FROM menu WHERE pageID = ? LIMIT 1");
		$stmt2->bindValue(1, $id, PDO::PARAM_STR);
		$stmt2->execute();
		while($row2 = $stmt2->fetch()){
			$page = $row2['form'];
		}

		if($page != ""){
		return "view/".$page;

		}else{
			return "view/member.php";
		}
	}
}



function getSideMenu($id){
	global $dbConn;
	//Is the user Logged in
	if(loginCheck() == true){

		if($id == ""){
			return null;
		}else{
			if(getAuth($id,$_SESSION['IDNumber']) == true){
				$stmt1 = $dbConn->prepare("SELECT form FROM menu WHERE pageID = ? LIMIT 1");
				$stmt1->bindValue(1, $id, PDO::PARAM_STR);
				$stmt1->execute();
				while($row1 = $stmt1->fetch()){
					$page = $row1['form'];
				}

				return "sidemenu/".$page;

			}else{
				return null;
			}
		}
	}
}


function getPageTitle($pageID){

	global $dbConn;

	$stmt1 = $dbConn->prepare("SELECT menuName FROM menu WHERE pageID LIKE :pageID");
	$stmt1->bindValue(':pageID', $pageID, PDO::PARAM_STR);
	$stmt1->execute();
	while($row1 = $stmt1->fetch()){
		return $title = $row1['menuName'];
	}

}

//Function to validate user input
function validateData($data){
	if($data == ""){
		return NULL;
	}else{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
}

function validateNumber($data){
	$data = preg_replace("/[A-Za-z,]/", "",$data);
	if(is_numeric($data)){
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		$data = str_replace(',','',$data)+0;
	}else{
		$data = NULL;
	}
	return $data;
}

	function camelBack($data){
		$position = strcspn($data, 'ABCDEFGHJIJKLMNOPQRSTUVWXYZ');

		if($position > 0){
			return $data = ucwords(substr_replace($data, ' ', $position, 0));
		}else{
			return $data = ucwords($data);
		}
	}


function getAuth($id,$user){

	global $dbConn;
	//GET AUTHORITY FROM PAGE
	$stmt1 = $dbConn->prepare("SELECT auth FROM menu WHERE pageID = ? LIMIT 1");
	$stmt1->bindValue(1, $id, PDO::PARAM_STR);
	$stmt1->execute();
	while($row1 = $stmt1->fetch()){
		$authPage = $row1['auth'];
	}
	$countPage = strlen($authPage);

	//GET AUTHORITY FROM USER
	$stmt2 = $dbConn->prepare("SELECT auth FROM users WHERE IDNumber = ? LIMIT 1");
	$stmt2->bindValue(1, $user, PDO::PARAM_STR);
	$stmt2->execute();
	while($row2 = $stmt2->fetch()){
		$authUser = $row2['auth'];
	}
	$countUser = strlen($authUser);
		    //Bug fix to ensure longer string is chosen
	$i=0;
	if($countPage >= $countUser && $countUser != 0){
		for($i == 0; $i < $countPage; $i++){
			if(preg_match("/$authPage[$i]/",$authUser)){
				return true;
			}
		}
	}else if($countPage <= $countUser && $countPage != 0){
		for($i == 0; $i < $countUser; $i++){
			if(preg_match("/$authUser[$i]/",$authPage)){
				return true;
			}
		}
	}
}

function getSidebar($pageInfo){
	$page = strpos($pageInfo,"/");
	$page = substr($pageInfo,$page+1);
	return "sidebar/".$page;
}

function paginate($reload, $page, $tpages, $adjacents) {

	$prevlabel = "&lsaquo; Prev";
	$nextlabel = "Next &rsaquo;";

	$out = "<div class=\"pagin\">\n";

	// previous
	if($page==1) {
		$out.= "<span>" . $prevlabel . "</span>\n";
	}
	elseif($page==2) {
		$out.= "<a href=\"" . $reload . "\">" . $prevlabel . "</a>\n";
	}
	else {
		$out.= "<a href=\"" . $reload . "" . ($page-1) . "\">" . $prevlabel . "</a>\n";
	}

	// first
	if($page>($adjacents+1)) {
		$out.= "<a href=\"" . $reload . "\">1</a>\n";
	}

	// interval
	if($page>($adjacents+2)) {
		$out.= "...\n";
	}

	// pages
	$pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
	$pmax = ($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages;
	for($i=$pmin; $i<=$pmax; $i++) {
		if($i==$page) {
			$out.= "<span class=\"current\">" . $i . "</span>\n";
		}
		elseif($i==1) {
			$out.= "<a href=\"" . $reload . "\">" . $i . "</a>\n";
		}
		else {
			$out.= "<a href=\"" . $reload . "" . $i . "\">" . $i . "</a>\n";
		}
	}

	// interval
	if($page<($tpages-$adjacents-1)) {
		$out.= "...\n";
	}

	// last
	if($page<($tpages-$adjacents)) {
		$out.= "<a href=\"" . $reload . "" . $tpages . "\">" . $tpages . "</a>\n";
	}

	// next
	if($page<$tpages) {
		$out.= "<a href=\"" . $reload . "" . ($page+1) . "\">" . $nextlabel . "</a>\n";
	}
	else {
		$out.= "<span>" . $nextlabel . "</span>\n";
	}

	$out.= "</div>";

	return $out;
}

function findexts ($filename) {
	$filename = strtolower($filename) ;
	$exts = split("[/\\.]", $filename) ;
	$n = count($exts)-1;
	$exts = $exts[$n];
return $exts;
}


function validateDate($date){
	if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)){
		    return $date;
		}
}


function GetAge($Birthdate){
	list($BirthYear,$BirthMonth,$BirthDay) = explode("-", $Birthdate);
	$YearDiff = date("Y") - $BirthYear;
	$MonthDiff = date("m") - $BirthMonth;
	$DayDiff = date("d") - $BirthDay;
	// If the birthday has not occured this year
	if ($DayDiff < 0 || $MonthDiff < 0)
	$YearDiff--;
	return $YearDiff;
}


function displayDate($date,$format){
	if($date != ""){
		$date = new DateTime($date);
		$formattedDate = $date->format($format);
		return $formattedDate;
	}
}

function getCurrency($currencyID) {

	global $dbConn;

	$stmt1 = $dbConn->prepare("select currency from currency WHERE currencyID = :currencyID LIMIT 0,1");
	$stmt1->bindValue(':currencyID', $currencyID, PDO::PARAM_STR);
	$stmt1->execute();
	while($row1 = $stmt1->fetch()) {
		$currency = $row1['currency'];
	}

	return $currency;
}

function uploadFile ($file_field = null, $check_image = false, $random_name = false,$extraName) {

	//Config Section
	//Set file upload path
	$path = 'images/all/'; //with trailing slash
	//Set max file size in bytes
	$max_size = 10000000;
	//Set default file extension whitelist
	$whitelist_ext = array('jpg','png','gif','jpeg','JPG','PNG','bmp');
	//Set default file type whitelist
	$whitelist_type = array('image/jpeg', 'image/png','image/gif','image/jpg','image/JPG','image/PNG','image/bmp');

	//The Validation
	// Create an array to hold any output
	$out = array('error'=>null);

	if (!$file_field) {
		$out['error'][] = "Please specify a valid form field name";
	}

	if (!$path) {
		$out['error'][] = "Please specify a valid upload path";
	}

	if (count($out['error'])>0) {
		return $out;
	}

	//Make sure that there is a file
	if((!empty($_FILES[$file_field])) && ($_FILES[$file_field]['error'] == 0)) {

		// Get filename
		$file_info = pathinfo($_FILES[$file_field]['name']);
		$name = $file_info['filename'];
		$ext = $file_info['extension'];

		//Check file has the right extension
		if (!in_array($ext, $whitelist_ext)) {
		  $out['error'][] = "Invalid file Extension";
		}

		//Check that the file is of the right type
		if (!in_array($_FILES[$file_field]["type"], $whitelist_type)) {
		  $out['error'][] = "Invalid file Type";
		}

		//Check that the file is not too big
		if ($_FILES[$file_field]["size"] > $max_size) {
		  $out['error'][] = "File is too big";
		}

		//If $check image is set as true
		if ($check_image) {
		  if (!getimagesize($_FILES[$file_field]['tmp_name'])) {
		    $out['error'][] = "Uploaded file is not a valid image";
		  }
		}

		//Create full filename including path
		if ($random_name) {
		  // Generate random filename
		  $tmp = str_replace(array('.',' '), array('',''), microtime());

		  if (!$tmp || $tmp == '') {
		    $out['error'][] = "File must have a name";
		  }
		  $newname = $extraName.'.'.$ext;
		} else {
		    $newname = $name.'.'.$ext;
		}

		//Check if file already exists on server


		if (count($out['error'])>0) {
		  //The file has not correctly validated
		  return $out;
		}

		if (file_exists($path.$newname))  {
				chmod($path.$newname,0755);
				unlink($path.$newname);



				if (move_uploaded_file($_FILES[$file_field]['tmp_name'], $path.$newname)) {
		 		 //Success
		  		$out['filepath'] = $path;
		  		$out['filename'] = $newname;
		  		$out['error'][] = "A file with this name already exists";
		  		return $out;
				} else {
		  		$out['error'][] = "Server Error!";
				}

		}

		if (move_uploaded_file($_FILES[$file_field]['tmp_name'], $path.$newname)) {
		  //Success
		  $out['filepath'] = $path;
		  $out['filename'] = $newname;
		  return $out;
		} else {
		  $out['error'][] = "Server Error!";
		}

	} else {
		$out['error'][] = "No file uploaded";
		return $out;
	}
}

function numberToWords($number){
	$words = array ('zero',
			'one',
			'two',
			'three',
			'four',
			'five',
			'six',
			'seven',
			'eight',
			'nine',
			'ten',
			'eleven',
			'twelve',
			'thirteen',
			'fourteen',
			'fifteen',
			'sixteen',
			'seventeen',
			'eighteen',
			'nineteen',
			'twenty',
			30=> 'thirty',
			40 => 'fourty',
			50 => 'fifty',
			60 => 'sixty',
			70 => 'seventy',
			80 => 'eighty',
			90 => 'ninety',
			100 => 'hundred',
			1000=> 'thousand');

	if (is_numeric ($number))
	{
		$number = (int) round($number);
		if ($number < 0)
		{
			$number = -$number;
			$number_in_words = 'minus ';
		}
		if ($number > 1000)
		{
			$number_in_words = $number_in_words . numberToWords(floor($number/1000)) . " " . $words[1000];
			$hundreds = $number % 1000;
			$tens = $hundreds % 100;
			if ($hundreds > 100)
			{
				$number_in_words = $number_in_words . ", " . numberToWords ($hundreds);
			}
			elseif ($tens)
			{
				$number_in_words = $number_in_words . " and " . numberToWords ($tens);
			}
		}
		elseif ($number > 100)
		{
			$number_in_words = $number_in_words . numberToWords(floor ($number / 100)) . " " . $words[100];
			$tens = $number % 100;
			if ($tens)
			{
				$number_in_words = $number_in_words . " and " . numberToWords ($tens);
			}
		}
		elseif ($number > 20)
		{
			$number_in_words = $number_in_words . " " . $words[10 * floor ($number/10)];
			$units = $number % 10;
			if ($units)
			{
				$number_in_words = $number_in_words . numberToWords ($units);
			}
		}
		else
		{
			$number_in_words = $number_in_words . " " . $words[$number];
		}
		return $number_in_words;
	}
	return false;
}

function convertToNull($emptyString){
	return ($emptyString === '') ? NULL : $emptyString;
}


	function telephone($telephone){
		//remove any spaces
		$telephone = str_replace(" ","",$telephone);
		//If telephone starts with +254
		if(preg_match("/^\+254[0-9]+$/", $telephone ) && preg_match("/\w{12,}/", $telephone  )){
			return $telephone;
		}else if(preg_match("/^254[0-9]+$/", $telephone ) && preg_match("/\w{12,}/", $telephone  )){
			$telephone = str_replace("254","+254",$telephone);
			return $telephone;
		}else if(preg_match("/^07[0-9]+$/", $telephone ) && preg_match("/\w{10,}/", $telephone  )){
			$pos = strpos($telephone, "07");
			if($pos !== false) {
				$telephone= substr_replace($telephone, "+2547", $pos, strlen("07"));
			}
			return $telephone;
		}else if(preg_match("/^01[0-9]+$/", $telephone ) && preg_match("/\w{10,}/", $telephone  )){
			$pos = strpos($telephone, "07");
			if($pos !== false) {
				$telephone= substr_replace($telephone, "+2541", $pos, strlen("01"));
			}
			return $telephone;
		}else{
			return null;
		}
	}

//Check if ID number meets the requisite minimum requirements
	function IDNumber($IDNumber){
		//remove any spaces
		$IDNumber = str_replace(" ","",$IDNumber);
		//Is it a passport
		if(preg_match("/^\[a-dA-D][0-9]+$/", $telephone ) && preg_match("/\w{12,}/", $telephone  )){
			return $telephone;
		}else if(preg_match("/^254[0-9]+$/", $telephone ) && preg_match("/\w{12,}/", $telephone  )){
			$telephone = str_replace("254","+254",$telephone);
			return $telephone;
		}else if(preg_match("/^07[0-9]+$/", $telephone ) && preg_match("/\w{10,}/", $telephone  )){
			$pos = strpos($telephone, "07");
			if($pos !== false) {
				$telephone= substr_replace($telephone, "+2547", $pos, strlen("07"));
			}
			return $telephone;
		}else if(preg_match("/^01[0-9]+$/", $telephone ) && preg_match("/\w{10,}/", $telephone  )){
			$pos = strpos($telephone, "07");
			if($pos !== false) {
				$telephone= substr_replace($telephone, "+2541", $pos, strlen("01"));
			}
			return $telephone;
		}else{
			return null;
		}
	}

	function sendSMS($phone,$message){

		global $dbConn;

		 $data = array(
			'recipients' => $phone,
			'message' => $message,
			'api' => '2ItWIFKWOXppnSnh3x0ZKKaCPqF1hzouqX76f2WHtSqBdZBZPB',
		);

		$url = "https://apps.plinysolutions.com/api/sms/";
		$num = 1;
		$ch = curl_init($url);
		$postString = http_build_query($data, '', '&');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($ch);
		$json = json_decode($response, true);


		$number = $json['number'];
		$status = $json['status'];
		$status_message = $json['status_message'];
		$message = $json['message_id'];
		$cost = $json['cost'];

		//Log sms
		try{
			$stmt1 = $dbConn->prepare("INSERT INTO sms(phone_number, message, status_code, status) VALUES (:phone_number, :message, :status_code, :status)");
			$stmt1->bindValue(':phone_number', $number, PDO::PARAM_STR);
			$stmt1->bindValue(':message', $message, PDO::PARAM_STR);
			$stmt1->bindValue(':status', $status_message, PDO::PARAM_STR);
			$stmt1->bindValue(':status_code', $status, PDO::PARAM_STR);
			$stmt1->execute();
		}catch(PDOException $err){
			error_log($err);
		}

		return $status . "@" . $status_message;
	 }

function downloadHeaders($filename) {
		// disable caching
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download
		header("Content-type: application/vnd.ms-excel");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");
		ob_end_clean();
}
?>
