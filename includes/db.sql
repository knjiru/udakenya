INSERT INTO menu(menuName,form,module,auth,pageID) VALUES ("Members","terms.php","member","","tm");


INSERT INTO menu(menuName,form,module,auth,pageID) VALUES ("Members","member.php","member","","mm");
INSERT INTO menu(menuName,form,module,auth,pageID) VALUES ("Success","success.php","member","","ss");

DROP TABLE IF EXISTS `members`;
 CREATE TABLE `member`(
	id INT AUTO_INCREMENT NOT NULL,
	names VARCHAR(255) DEFAULT NULL,
	id_no VARCHAR(15) NOT NULL,
	mobile_no VARCHAR(15) DEFAULT NULL,
	constituency_id INT DEFAULT NULL,
	county_id INT DEFAULT NULL,
	created_by VARCHAR(100) DEFAULT NULL,
	date_created DATETIME DEFAULT NULL,
	updated_by VARCHAR(100) DEFAULT NULL,
	date_updated DATETIME DEFAULT NULL,
	PRIMARY KEY (id),
	UNIQUE KEY unq_member_idno(id_no)
)DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;



CREATE DATABASE udakenya;

use udakenya;

CREATE USER 'udakenya'@'localhost' IDENTIFIED BY '3FsT@1GJuqGyqD';
GRANT SELECT, DELETE, UPDATE, CREATE, INSERT, ALTER ON udakenya.* TO 'udakenya'@'localhost';
FLUSH PRIVILEGES;




CREATE TABLE members(
	id INT AUTO_INCREMENT NOT NULL,
	names VARCHAR(255) DEFAULT NULL,
	id_no VARCHAR(15) NOT NULL,
	mobile_no VARCHAR(15) DEFAULT NULL,
	constituency_id INT DEFAULT NULL,
	county_id INT DEFAULT NULL,
	created_by VARCHAR(100) DEFAULT NULL,
	date_created DATETIME DEFAULT NULL,
	updated_by VARCHAR(100) DEFAULT NULL,
	date_updated DATETIME DEFAULT NULL,
	PRIMARY KEY (id),
	UNIQUE KEY unq_member_idno(id_no)
)DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

ALTER TABLE member	 ADD CONSTRAINT MEMBER_FK_CONSTITUENCY FOREIGN KEY (constituency_id) REFERENCES constituency (id);
ALTER TABLE member	 ADD CONSTRAINT MEMBER_FK_COUNTY FOREIGN KEY (county_id) REFERENCES county (id);

CREATE TABLE users(
	IDNumber VARCHAR(40) PRIMARY KEY NOT NULL,
	surname VARCHAR(100),
	firstName VARCHAR(100),
	otherNames VARCHAR(100),
	designation VARCHAR(100),
	category VARCHAR(100),
	dateBirth DATE,
	gender VARCHAR(15),
	station VARCHAR(100),
	dateJoined DATE,
	telephone VARCHAR(30),
	email VARCHAR(100),
	status VARCHAR(30),
	notes VARCHAR(100),
	pswd CHAR(128),
	salt CHAR(128),
	auth VARCHAR(15),
	createdBy VARCHAR(100),
	updatedBy VARCHAR(100),
	dateCreated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dateUpdated TIMESTAMP NOT NULL default now() on update now()
);

CREATE TABLE login_attempts(
	IDNumber varchar(25),
	timeDone timestamp,
	createdBy VARCHAR(254),
	updatedBy VARCHAR(254),
	dateCreated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dateUpdated TIMESTAMP NOT NULL default now() on update now(),
	PRIMARY KEY(IDNumber, timeDone),
	FOREIGN KEY (IDNumber) REFERENCES users(IDNumber) ON UPDATE CASCADE
);

CREATE TABLE menu(
	pageID VARCHAR(15) PRIMARY KEY,
	menuName VARCHAR(140),
	form VARCHAR(254),
	type VARCHAR(50),
	module VARCHAR(254),
	auth VARCHAR(254),
	createdBy VARCHAR(254),
	updatedBy VARCHAR(254),
	dateCreated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	dateUpdated TIMESTAMP NOT NULL default now() on update now()
);


CREATE TABLE constituency(
	id INT AUTO_INCREMENT NOT NULL,
	code varchar(3),
	county_id INT,
	name VARCHAR(255),
	voters int,
	created_by VARCHAR(100) DEFAULT NULL,
	date_created DATETIME DEFAULT NULL,
	updated_by VARCHAR(100) DEFAULT NULL,
	date_updated DATETIME DEFAULT NULL,
	PRIMARY KEY (id),
	UNIQUE KEY (code)
);

INSERT INTO constituency (code, countyID, name
