<?php
	require_once("includes/dbconf.php");
	require_once("includes/functions.php");
	//require_once("includes/levakFunctions.php");
	//require_once("includes/resize-class.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<!-- Include Meta Data -->
		<meta http-eqsuiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
		<meta property="og:title" content="UDA Member registration platform" />
		<meta property="og:description" content="UDa Member Registration Form" />
		<meta property="og:url" content="https://udakenya.org/" />
		<meta property="og:image" itemprop="image" content="https://udakenya.org/images/UDA-web.png" />
		<meta property="og:type" content="website" />
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@udakenya">
		<meta name="twitter:creator" content="@udakenya">
<meta name="twitter:title" content="UDA Member registration platform">
		<meta name="twitter:image" content="https://udakenya.org/images/UDA-web.png">
		<!--End Meta data -->

		<link rel="icon" type="image/png" sizes="32x32" href="<?php echo $base_url ?>/images/favicon.png">

	<!-- Include CSS Stylesheets -->
	<link rel="stylesheet" href="<?php echo $base_url?>/css/main.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo $base_url?>/css/members.css" type="text/css" media="screen" />


	<!-- Include Scripts -->
	<script type="text/javascript" src="<?php echo $base_url?>/scripts/jquery.min.js"></script>

	<script type="text/javascript" src="<?php echo $base_url?>/scripts/extras.js"></script>

