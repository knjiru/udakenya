<?php
	ini_set('display_errors',0);
	$base_url = "/udakenya";
	include("includes/functions.php");
	secureSessionStart();
	include("includes/headers.php");
	$redirect = urlencode("http" . ((!empty($_SERVER['HTTPS'])) ? "s" : "") . "://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
	$page = validateData($_GET['id']);
	$pageData = getPage($page);
?>

	<title>United Democratic Alliance- <?php echo getPageTitle($page)?></title>
	</head>
	<body>
		<table  border = '0' align='center' >
			<tr>
				<td>
					<div class="topmenu"></div>
				</td>
			</tr>
			<tr>
				<td style="vertical-align:top">
					<div class="workspace"><?php include($pageData);?></div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="footer"><?php include('view/footer.php');?></div>
				</td>
			</tr>
		</table>
	</body>
</html>
