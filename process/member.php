<?php

if($process == 'member'){


	$mobile_number = $id_number = $names = $county = $constituency  = "";


	$id_number = validateData($_POST['id_number']);
	$names = validateData($_POST['names']);
	$mobile_number = validateData($_POST['mobile_number']);
	$county = validateData($_POST['county']);
	$constituency = validateData($_POST['constituency']);



	$mobile_number1 = telephone($mobile_number);
	//$id_number1 = IDNumber($id_number);

	//error checking
	if($names == '' ){
		$message .= 'Name cannot be blank<br>';
		$error += 1;
	}

	if($mobile_number == '' ){
		$message .= 'Mobile Number cannot be blank<br>';
		$error += 1;
	}

	if($id_number == ''){
		$message .= 'ID Number cannot be blank<br>';
		$error += 1;
	}

	if($mobile_number1 == '' ){
		$message .= 'Please enter a valid telephone number<br>';
		$error += 1;
	}

/*	if($id_number1 == ''){
		$message .= 'Please enter a valid ID Number<br>';
		$error += 1;
	}
*/

//ID has to be unique
	$stmt2 = $dbConn->prepare("SELECT id_no FROM member WHERE id_no = :id_number");
	$stmt2->bindValue(':id_number', $id_number, PDO::PARAM_STR);
	$stmt2->execute();
	if($stmt2->rowCount() > 0) {
		$message .= 'Your ID Number is already registered<br>';
		$error += 1;
	}

	if($error == 0){

		//Enter data into database
		try{
			$stmt1 = $dbConn->prepare("INSERT INTO member(mobile_no, id_no, names, county_id, constituency_id, created_by, date_created, date_updated) VALUES (:mobile_number, :id_number, :names, :county, :constituency, :created_by, :date_created, :date_updated)");
			$stmt1->bindValue(':mobile_number', $mobile_number, PDO::PARAM_STR);
			$stmt1->bindValue(':id_number', $id_number, PDO::PARAM_STR);
			$stmt1->bindValue(':names', $names, PDO::PARAM_STR);
			$stmt1->bindValue(':county', $county, PDO::PARAM_STR);
			$stmt1->bindValue(':constituency', $constituency, PDO::PARAM_STR);
			$stmt1->bindValue(':created_by', 'online', PDO::PARAM_STR);
			$stmt1->bindValue(':date_created', date("Y-m-d H:i:s"), PDO::PARAM_STR);
			$stmt1->bindValue(':date_updated', date("Y-m-d H:i:s"), PDO::PARAM_STR);
			$stmt1->execute();

			include("view/success.php");
			$message .= "Success Registration";
			exit();

		}catch(PDOException $err2){
			$message  .= 	 "Sorry we experienced an error while processing your data <br>";
			$error += 1;
			include("view/member.php");
			exit();
		}


	}else{
		include("view/member.php");
		exit();
	}
}
?>
