

	<div class="message"><?php echo $message ?></div>

	<div class="fullLength view">
	<div class="img-right"><img src="images/uda_logo_small.png"></div>
	<div class="header"> UDA Membership Registration Form</div>
	<form name="form" action="<?php echo $base_url?>/success" method="post">
	<input type='hidden' name = "process" value="member">
	<div  class="holder">
		<div  class="text">ID Number*:</div>
		<div class="flat"><input type='text' required name = 'id_number' value= "<?php echo $id_number?>"></div>
	</div>
	<div  class="holder">
		<div  class="text">Names*:</div>
		<div class="flat"><input type='text' required name = 'names' value= "<?php echo $names?>"></div>
	</div>
	<div  class="holder">
		<div  class="text">Telephone*:</div>
		<div class="flat"><input type='text' required name = 'mobile_number' value= "<?php echo $mobile_number?>"></div>
	</div>
	<div  class="holder">
		<div  class="text">County:</div>
		<div class="flat">
		<select name="county"  id="county">
			<option value="0">- Select -</option>
			<option value ="30" <?php if($county == "30" ){ echo "selected='selected' " ;}?>>BARINGO </option>
			<option value ="36" <?php if($county == "36" ){ echo "selected='selected' " ;}?>>BOMET </option>
			<option value ="39" <?php if($county == "39" ){ echo "selected='selected' " ;}?>>BUNGOMA </option>
			<option value ="40" <?php if($county == "40" ){ echo "selected='selected' " ;}?>>BUSIA </option>
			<option value ="48" <?php if($county == "48" ){ echo "selected='selected' " ;}?>>DIASPORA </option>
			<option value ="28" <?php if($county == "28" ){ echo "selected='selected' " ;}?>>ELGEYO/MARAKWET</option>
			<option value ="14" <?php if($county == "14" ){ echo "selected='selected' " ;}?>>EMBU </option>
			<option value ="07" <?php if($county == "07" ){ echo "selected='selected' " ;}?>>GARISSA </option>
			<option value ="43" <?php if($county == "43" ){ echo "selected='selected' " ;}?>>HOMA BAY </option>
			<option value ="11" <?php if($county == "11" ){ echo "selected='selected' " ;}?>>ISIOLO </option>
			<option value ="34" <?php if($county == "34" ){ echo "selected='selected' " ;}?>>KAJIADO </option>
			<option value ="37" <?php if($county == "37" ){ echo "selected='selected' " ;}?>>KAKAMEGA </option>
			<option value ="35" <?php if($county == "35" ){ echo "selected='selected' " ;}?>>KERICHO </option>
			<option value ="22" <?php if($county == "22" ){ echo "selected='selected' " ;}?>>KIAMBU </option>
			<option value ="03" <?php if($county == "03" ){ echo "selected='selected' " ;}?>>KILIFI </option>
			<option value ="20" <?php if($county == "20" ){ echo "selected='selected' " ;}?>>KIRINYAGA </option>
			<option value ="45" <?php if($county == "45" ){ echo "selected='selected' " ;}?>>KISII </option>
			<option value ="42" <?php if($county == "42" ){ echo "selected='selected' " ;}?>>KISUMU </option>
			<option value ="15" <?php if($county == "15" ){ echo "selected='selected' " ;}?>>KITUI </option>
			<option value ="02" <?php if($county == "02" ){ echo "selected='selected' " ;}?>>KWALE </option>
			<option value ="31" <?php if($county == "31" ){ echo "selected='selected' " ;}?>>LAIKIPIA </option>
			<option value ="05" <?php if($county == "05" ){ echo "selected='selected' " ;}?>>LAMU </option>
			<option value ="16" <?php if($county == "16" ){ echo "selected='selected' " ;}?>>MACHAKOS </option>
			<option value ="17" <?php if($county == "17" ){ echo "selected='selected' " ;}?>>MAKUENI </option>
			<option value ="09" <?php if($county == "09" ){ echo "selected='selected' " ;}?>>MANDERA </option>
			<option value ="10" <?php if($county == "10" ){ echo "selected='selected' " ;}?>>MARSABIT </option>
			<option value ="12" <?php if($county == "12" ){ echo "selected='selected' " ;}?>>MERU </option>
			<option value ="44" <?php if($county == "44" ){ echo "selected='selected' " ;}?>>MIGORI </option>
			<option value ="01" <?php if($county == "01" ){ echo "selected='selected' " ;}?>>MOMBASA </option>
			<option value ="21" <?php if($county == "21" ){ echo "selected='selected' " ;}?>>MURANG'A </option>
			<option value ="47" <?php if($county == "47" ){ echo "selected='selected' " ;}?>>NAIROBI </option>
			<option value ="32" <?php if($county == "32" ){ echo "selected='selected' " ;}?>>NAKURU </option>
			<option value ="29" <?php if($county == "29" ){ echo "selected='selected' " ;}?>>NANDI </option>
			<option value ="33" <?php if($county == "33" ){ echo "selected='selected' " ;}?>>NAROK </option>
			<option value ="46" <?php if($county == "46" ){ echo "selected='selected' " ;}?>>NYAMIRA </option>
			<option value ="18" <?php if($county == "18" ){ echo "selected='selected' " ;}?>>NYANDARUA </option>
			<option value ="19" <?php if($county == "19" ){ echo "selected='selected' " ;}?>>NYERI </option>
			<option value ="49" <?php if($county == "49" ){ echo "selected='selected' " ;}?>>PRISONS </option>
			<option value ="25" <?php if($county == "25" ){ echo "selected='selected' " ;}?>>SAMBURU </option>
			<option value ="41" <?php if($county == "41" ){ echo "selected='selected' " ;}?>>SIAYA </option>
			<option value ="06" <?php if($county == "06" ){ echo "selected='selected' " ;}?>>TAITA TAVETA </option>
			<option value ="04" <?php if($county == "04" ){ echo "selected='selected' " ;}?>>TANA RIVER </option>
			<option value ="13" <?php if($county == "13" ){ echo "selected='selected' " ;}?>>THARAKA - NITHI</option>
			<option value ="26" <?php if($county == "26" ){ echo "selected='selected' " ;}?>>TRANS NZOIA </option>
			<option value ="23" <?php if($county == "23" ){ echo "selected='selected' " ;}?>>TURKANA </option>
			<option value ="27" <?php if($county == "27" ){ echo "selected='selected' " ;}?>>UASIN GISHU </option>
			<option value ="38" <?php if($county == "38" ){ echo "selected='selected' " ;}?>>VIHIGA </option>
			<option value ="08" <?php if($county == "08" ){ echo "selected='selected' " ;}?>>WAJIR </option>
			<option value ="24" <?php if($county == "24" ){ echo "selected='selected' " ;}?>>WEST POKOT </option>
		</select>
		</div>
	</div>
	<div  class="holder">
		<div  class="text">Constituency:</div>
		<div class="flat">
			<select name="constituency"  id="constituency">
				<option value="0">- Select -</option>
			</select>
		</div>
	</div>


</body>
</html>
	<div  class="holder">
		<div class="text"><input type='checkbox' name = 'accept' required  checked="checked" value= "">I agree to the <a href="<?php echo $base_url?>/terms">Terms and Conditions</a></div>
	</div>
	<div  class="fullLength title">
		<input type="submit" value="Register" class="buttonSubmit" size="40"  />
	</div>
	</form>
</div>
</div>
