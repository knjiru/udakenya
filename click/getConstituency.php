<?php
	require_once("../includes/dbconf.php");

$countyID = 0;

if(isset($_POST['county'])){
   $countyID = $_POST['county']; // department id
}


$users_arr = array();

if($countyID != ""){
    //$sql = "SELECT constituencyCode, constituencyName FROM constituency WHERE countyName=".$linkName;
		$stmt2 = $dbConn->prepare("SELECT code, name FROM constituency WHERE county_id = :countyName");
		$stmt2->bindValue(':countyName', $countyID, PDO::PARAM_STR);
		$stmt2->execute();

		while($row2 = $stmt2->fetch()) {
			$name = $row2['name'];
			$code = $row2['code'];

        $users_arr[] = array("id" => $code, "name" => $name);
    }
}

// encoding array to json format
echo json_encode($users_arr);
